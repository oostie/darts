#Lets Play Darts!



On this website you can track your darts scores.

On your first visit you can enter the player names and select a game type.
These settings are stored on your PC so you won't loose these on a page refresh also the Sets and Legs are saved.

At this moment your scores aren't saved you should still be careful with your refresh button.

##Features

* Checkout tips.
* Ipad ready.
* Sets and Legs are saved.


##Bug tracker

Have a bug? Please create an issue on Bitbucket at http://git.oostie.nl/darts/issues/