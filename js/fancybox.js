$(document).ready(function() {
  // On click function in the fancybox.
  $('#submit-names').click(function() {
    // Replace player1 and player 2 with submitted names.
    $('.name_player_1').text($('#player-1-name').val() || 'player1');
    $('.name_player_2').text($('#player-2-name').val() || 'player2');
    $('.start-with').text($('#game-type').val());

    var settings = JSON.parse(localStorage.getItem('settings'));
    settings.players.player_1.name = $('#player-1-name').val() || 'player1';
    settings.players.player_2.name = $('#player-2-name').val() || 'player2';
    settings.options.game_type = $('#game-type').val();
    localStorage.setItem('settings',JSON.stringify(settings));


    $('#header-game-type').text($('#game-type').val());
    // Set status to 1.
    localStorage.setItem('status', '1');
    // Then close the fancybox.
    $.fancybox.close();
    // Focus on first input.
    // Later changes by player who wins the bull.
    $('.score_input_1').focus();
  });

  // If there are no settings saved open new game fancybox.
  if (!localStorage.getItem('status')) {
    // Submit on Enter.
    $('#settings input').keyup(function(e) {
      if(e.keyCode == 13) {
        $('#submit-names').click();
      }
    });
  }
  // Open the given file in a fancybox.
  $('.fancybox').click(function() {
    $.pageslide.close()
    $.fancybox({
      'href' : $(this).attr("data-id"),
      'type'         : 'iframe',
      'autoSize' : false,
      'width' : 500,
      'height' : 500
    });
  });
});
