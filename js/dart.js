$(document).ready(function() {

  // Create the settings object if not exists.
  if (localStorage.getItem('status')) {
    showContainer();
    processScores();
  } else {
    showHome();
  }

  // Load latest tag from bitbucket.
  getBitbucketTag();
  var settings = getLocalStorage();
  $('input[type=number]').keyup(function(e) {
    if(e.keyCode == 13) {
      $(this).next('a.submit_button').click();
    }
  });

  // Show or hide tooltip on typing.
  $('input[type=number]').keyup(function(e) {
    var $input = $(this);
    var this_name = $input.attr('name');
    var this_id = this_name.replace('score_input_', '');
    var left = $('ul.player_' + this_id).find('span.left').text();
    var future_score = parseFloat(left) - parseFloat($(this).val());

    if (parseFloat(future_score) > 0) {
    if ((future_score in checkouts)) {
      var checkout = '<br />' + checkouts[future_score];
    }
      $input.tooltipster({
        position : 'right'
      });
      $input.tooltipster('update', future_score + checkout);
      $input.tooltipster('show');

      if (e.keyCode == 13) {
        $input.tooltipster('destroy');
      }
    } else {
      $input.tooltipster('destroy');
    }
  });
  // Hide tooltips on blur.
  $('input[type=number]').blur(function(e) {
    $('body').tooltipster('hide');
  });

  // Add turn indicator.
  $('input.score_input').focus(function() {
    var $wrapper = $(this).parents('.player').addClass('focussed');
  }).blur(function() {
    var $wrapper = $(this).parents('.player').removeClass('focussed');
  });

  // On click function in when adding scores.
  $('.submit_button').bind('click', function() {
    // Set variables.
    var player_id = $(this).attr('id');
    var score = $(this).prev().val();
    var score_left = $('ul.' + player_id + ' .left').text() - score;

    // Only do things when there is a number typed.
    if (score != '' && !isNaN(score) && score < 181 ) {
      if (score_left > 1) {
        // Only do math when your havent died.
        scoreStorage(player_id, score);
        addScore(player_id, score, 'manual');
      }
      else if (score_left == 0) {
        addLeg(player_id);
      }
      else if (score_left == 1 || score_left < 0){
        setMessage(player_id, 'error', 'No score!');
      }
      $('.score_input').val('');
    }
    else {
      $(this).prev('input').val('');
      setMessage(player_id,'error', 'Pleas add a correct number!')
    }
  });

  /**
   * Add a score to the given player.
   */
  function addScore(player_id, score, type) {
    calculateAverage(player_id);
    setFocus('score');
    var $list = $('ul.' + player_id);
    var current_score = $('ul.' + player_id + ' span.left').text();

    var score_left = current_score - score;
    // If there's a checkout set a message.
    if ((score_left in checkouts)) {
      setMessage(player_id, 'notice', checkouts[score_left])
    }

    var $new_score = '<li class="custom clearfix">';
    $new_score +=     '<span class="delete"><a href="#"><img title="delete" src="css/images/icon_delete.png"></img></a></span>';
    $new_score +=     '<span class="score">' + score + '</span>';
    $new_score +=     '<span class="left">'+ score_left +'</span>';
    $new_score +=    '</li>';
    $('ul.' + player_id + ' .left')
      .addClass('done')
      .removeClass('left')
      .animate({
        fontSize : '15px',
        opacity : '0.3',
      },'fast')

    if ($('ul.' + player_id + ' li.custom').size() > 7) {
      if (type == 'manual') {
        $list.children().filter(':visible').eq(0).slideUp(400,function (){
          $list.append($new_score).children(':last');
        });
      }
      else {
        $list.children().filter(':visible').eq(0).hide(0,function (){
          $list.append($new_score).children(':last');
        });
      }
    }
    else {
      $list.append($new_score).children(':last');
    }
  }

  $(document).on('click','.delete a',function(event){
    var $list_item = $(this).parents('li');
    var $list = $list_item.parent('ul');
    var player_id = $list.attr('class').split(' ')[0]
    var list_item_pos = $(this).parents('li').index();

    if($list_item.children().hasClass('left')) {
      $list_item.prev('li').find('span.done').addClass('left').removeClass('done');
      $('.left', $list).animate({opacity : '1',fontSize : '24px'}, 100);
    } else {
      var score_to_delete = $list_item.find('.score').text();
      var score_before_delete = $('.left', $list).text();
      var new_score = (parseInt(score_before_delete) +	 parseInt(score_to_delete));
      $('.left', $list).text(new_score);
    }
    delete_score(player_id, list_item_pos);
    $list_item.slideUp();
  });

  /**
   * Function to delete scores from the score object.
   */
  function delete_score(player_id, list_item_pos) {
    var new_scores = {};
    var settings_index = Object.keys(settings.players[player_id].scores).length;

    if (settings_index == 1) {
      settings.players[player_id].scores = {};
    }
    else {
      $.each(settings.players[player_id].scores, function( key, value ) {
        if (key == list_item_pos) {
          var new_total_scores = settings.players[player_id].total_scores - parseFloat(value);
          settings.players[player_id].total_scores = new_total_scores;
          return true;
        } else {
          var new_score_index = Object.keys(new_scores).length +1;
          new_scores[new_score_index] = value;
        }
        settings.players[player_id].scores = new_scores;
      });

      settings.players[player_id].total_scores = settings.players[player_id].turns - 1;
      calculateAverage(player_id);
    }
    storageUpdate(settings);
  }

  /**
   * Add a leg to the given player.
   */
  function addLeg(player_id) {
    var $leg_container = $('.scores_' + player_id + ' .leg');
    var leg = parseFloat($leg_container.text());

    $leg_container.text(leg += 1);

    legStorage(player_id, leg);
    scoreStorage('', 'clear');
    resetScores();

    // When the player has won 3 legs add a set.
    if ($leg_container.text() == '3') {
      $('.leg').text('0');
      legStorage('clear', '0');
      addSet(player_id);
    }
  }

  /**
   * Add a set to the given player.
   */
  function addSet(player_id) {
    var $set_container = $('.scores_' + player_id + ' .set');
    var set = parseFloat($set_container.text());
    $set_container.text(set += 1);
    setStorage(player_id, set);
  }

  /**
   * Function to generate messages.
   */
  function setMessage(player_id,type, $message) {
    if (type == 'notice') {
      $container = $('.' + player_id + ' ul.message li.notice');
      $container.text($message).fadeIn();
    }
    else if (type == 'error' && $('li.error').size() < 1) {
      $container = $('.' + player_id + ' ul.message');
      $container.prepend('<li class="'+ type +'">' + $message + '</li>');
      $('li.error').fadeOut(1000, function() {
        $(this).remove();
      });
    };
  };

  /**
   * Function to process all scores on load.
   */
  function processScores() {
    var settings = getLocalStorage();
    var score_count = Object.keys(settings.players.player_1.scores).length;

    $.each( settings.options, function( option, value ) {
      $('#header-game-type').text(settings.options.game_type);
      $('.start-with').text(settings.options.game_type);
    });

    $.each( settings.players, function( player, value ) {
      $('.name_' + player).text(settings.players[player].name || 'Player');
      $('.scores_' + player + ' .leg').text(settings.players[player].legs || 0);
      $('.scores_' + player + ' .set').text(settings.players[player].set || 0);

      calculateAverage(player);
      $.each( settings.players[player].scores, function(index, val) {
        setTimeout(function() {
          addScore(player, val, 'auto');
        }, index * 200);
      });
    });
  }
  /**
   * Set everything to default.
   */
  function resetScores() {
    // Reset scores.
    $('li.custom').remove();
    $('.score_table li').show();
    $('ul.message li.notice').text('Game On!');
    $('.score_table li span.start-with')
      .addClass('left')
      .removeClass('done')
      .css({
        opacity : '1',
        fontSize : '24px'
    });
  }
  /**
   * Needs Work.
   */
  function setFocus(type) {
    var $player_1 = $('input.score_input_1');
    var $player_2 = $('input.score_input_2');

    if (type == 'score') {
      var player = $('input.score_input:focus').attr('name');
      var $input = player == 'score_input_1' ? $player_2 : $player_1;
    }
    else {
      var $input = localStorage.getItem(type + '_starter') == 1 ? $player_2 : $player_1;
      var new_starter = localStorage.getItem(type + '_starter') == 1 ? 2 : 1;
      localStorage.setItem(type + '_starter', new_starter);
    }
    $input.focus();
  }

  /**
   * Update the localStorage.
   */
  function storageUpdate(storage) {
    localStorage.setItem('settings',JSON.stringify(storage));
  }

  /**
   * Store the scores in the local storage
   */
  function scoreStorage(player, score) {
    var settings = getLocalStorage();
    if (score == 'clear') {
      settings.players.player_1.scores = {};
      settings.players.player_2.scores = {};
    }
    else {
      var index = Object.keys(settings.players[player].scores).length +1;
      settings.players[player].scores[index] = score;
      settings.players[player].turns += parseFloat(1);
      var total_score = parseFloat(settings.players[player].total_scores) + parseFloat(score);
      settings.players[player].total_scores = total_score;
    }
    setLocalStorage(settings);
  }

  /**
   * Calculate average scores for given player.
   */
  function calculateAverage(player) {
    var settings = getLocalStorage();
    var total_score = parseInt(settings.players[player].total_scores);
    var turns = parseInt(settings.players[player].turns);
    var average = (total_score / turns);
    average = average.toFixed(1);

    if (!isNaN(average)) {
      $('.scores_' + player ).find('.avg_score').text(average);
      settings.players[player].avg = average;
    }
    setLocalStorage(settings);
  }

  /**
   * Store the scores in the local storage
   */
  function legStorage(player, leg) {
    var settings = getLocalStorage();
    if (player == 'clear') {
      settings.players.player_1.legs = 0;
      settings.players.player_2.legs = 0;
    }
    else {
      settings.players[player].legs = leg;
    }

    setFocus('leg');
    setLocalStorage(settings);
  }

  /**
   * Create an object with all settings.
   */
  function setSettings() {
    var settings = {
      options : {
        game_type : $('#game-type').val(),
        leg_starter : 1,
        set_starter : 1,
        message : 1
      },
      players : {
        player_1 : {
          name : $('.player .name_player_1').text(),
          legs : $('.scores_player_1 .leg').text(),
          sets : $('.scores_player_1 .set').text(),
          turns : 0,
          total_scores : 0,
          avg : 0,
          scores : {}
        },
        player_2 : {
          name : $('.player .name_player_2').text(),
          legs : $('.scores_player_2 .leg').text(),
          sets : $('.scores_player_2 .set').text(),
          turns : 0,
          total_scores : 0,
          avg : 0,
          scores : {}
        }
      }
    };
    // Save the settings
    setLocalStorage(settings);
    localStorage.setItem('leg_starter','1');
    localStorage.setItem('set_starter','1');
  }
  /**
   * Store the scores in the local storage
   */
  function setStorage(player, set) {
    var settings = getLocalStorage();
    setFocus('set');
    settings.players[player].set = set;
    setLocalStorage(settings);
  }

  /**
   * Store the scores in the local storage
   */
  function setLocalStorage(settings) {
    localStorage.setItem('settings',JSON.stringify(settings));
  }

  /**
   * Get the localstorage and return as a object.
   */
  function getLocalStorage() {
    settings = JSON.parse(localStorage.getItem('settings'));
    return settings;
  }

  /**
   * Function to reset fields and clear the localstorage.
   * And clear all variable html.
   */
  function clearLocalStorage() {
    $('.name_player_1').text('Player 1');
    $('.name_player_2').text('Player 2');
    $('.leg').text('0');
    $('.set').text('0');
    $('.avg_score').text('0');
    // Clear the storage and refresh.
    localStorage.clear();
    location.reload();
  }

  // Clear localstorage when clicking on
  // the clear button.
  $('#clear').click(function() {
    clearLocalStorage()
    showContainer();
  });
  // Show score forms on start.
  $('#start').click(function() {
    showContainer();
    setSettings();
    setTimeout(function(){
      $.fancybox({
        'href' : '#settings',
        'titleShow' : false,
        'openSpeed' : 'fast',
        'closeSpeed' : 'fast',
        'closeBtn' : false,
        keys : {
          close  : null
        },
        helpers : {
          overlay : {
            closeClick: false
          }
        },
        beforeShow : function() {
          this.title = 'Enter player names and game type';
          $('#game-type').val('501');
        },
        afterLoad: function() {
          setTimeout(function(){
            // Set focus on the first input item
            // with a delay otherwise it wont work.
            $('#player-1-name').focus();
          },100);
        },
      });
    },1000);
    setTimeout(function(){
      $('.video-background').videobackground('destroy');
    },500);
    $('.video-background').slideUp(200);
  });

  /**
   * function to hide home screen and show score forms
   */
  function showContainer() {
    $('#home').fadeOut(500);
    $('.container').delay(500).fadeIn();
    $('header').fadeIn(1000);
  }
  function showHome() {
    $('#home').slideDown(500);
    $('.container').delay(500).slideUp();
    $('header').delay(500).hide(1000);
		$('body').prepend('<div class="video-background"></div>');
		$('.video-background').videobackground({
			videoSource: [['http://dart-score.com/video.mp4', 'video/mp4']],
			controlPosition: '#article',
			loop: true,
			resizeTo: 'window',
			  preloadCallback: function() {
				$(this).videobackground('mute');
			}
		});
  }

  // Activate the pageslide.
  $('.ps_trigger').pageslide({
    direction: 'right'
  });

  /**
   * Function to get the latest bitbucket tag.
   */
  function getBitbucketTag() {
    var node = {};
    var bb_url = 'https://api.bitbucket.org/1.0/repositories/oostie/darts/tags/';

    // Get all tags from bitbucket.
    $.ajax({
      url: bb_url,
      dataType: 'jsonp',
      crossDomain: true,
      success: function (returndata){
        $.each(returndata, function(key, value){
          node[key] = {
            tag: key,
            message: value.message
          };
        });
        // Get all keys.
        var keys = $.map(node, function(item, key) {
          return key;
        });
        // Get the key with the highest value.
        newest = keys.sort(function (a, b) {
          return a > b ? 1 : (a < b ? -1 : 0);
        }).slice(-1);
        // Append the version tag.
        $('li.version').html('<p> Version: ' + node[newest].tag + '</p>');
      }
    });
  }
});
